def first_occurrence_index(target_value, iterable):
    for i, value in enumerate(iterable):
        if value == target_value:
            return i
    raise LookupError()
