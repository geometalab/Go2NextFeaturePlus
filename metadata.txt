# Mandatory items:

[general]
name=Go2NextFeaturePlus
qgisMinimumVersion=2.99
qgisMaximumVersion=3.99
version=3.0.1

author=Diego Fantino, Christopher Hilfing, Hanna Wildermuth, Stefan Keller, Alberto De Luca (for Tabacco Editrice)
email=geometalab@gmail.com

description=Allows jumping from a feature to another following an attribute order

about=This plugin allows to select a layer and jump from a feature to the next. It's possible to select an attribute of the layer (e.g. name) which defines the ordering. Besides the Pan function there's a Pan and Zoom function for polygons and lines and other options. In addition to the mentioned functionality it is also possible to set and persist a state ("fixed", "already fixed", "no issue", "unknown" or "undefined") of each feature. For this the layer needs to be updatable (which excludes e.g. formats like CSV or GeoJSON which need to be converted e.g. to GeoPackage).

repository=https://gitlab.com/geometalab/Go2NextFeaturePlus

# Recommended items:

tracker=https://gitlab.com/geometalab/Go2NextFeaturePlus/-/issues
homepage=https://gitlab.com/geometalab/Go2NextFeaturePlus

tags=python, osm, openstreetmap, vector, reconciliation
category=Vector
icon=icon.png
experimental=True
deprecated=False

# Changelog
changelog=
    3.0.1 2024-06: Update metadata
    3.0.0 2024-06: When copying attributes no longer quote the values and omit annotations and missing attributes.
    2.1.0 2024-06: Added functionality to copy attributes in key=value style and introduce states.
    2.0.0 2024-06: Migrated to QGIS3.
    